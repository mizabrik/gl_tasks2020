#include "screen.h"

#include <stdexcept>

#include "gl_helpers.h"

static const GLuint kPos2DAttribLocation = 0;

static const char *vertex_shader_source = R"(#version 420 core
in vec2 a_pos2d;

out vec2 tex_coord;

void main() {
  gl_Position = vec4(a_pos2d, 0.0, 1.0);
  tex_coord = vec2(0.5 * (1 + a_pos2d.x), 0.5 * (1 + a_pos2d.y));
}
)";

static const char *fragment_shader_source = R"(#version 420 core
in vec2  tex_coord;
uniform sampler2D tex;

out vec4 color;

void main() {
  color = texture(tex, tex_coord);
}
)";

Screen::Screen(GLuint texture_unit) {
  Shader vertex_shader(GL_VERTEX_SHADER, vertex_shader_source);
  if (!vertex_shader.Check()) {
    throw std::runtime_error("Could not create Screen: vertex shader compilation failed");
  }
  Shader fragment_shader(GL_FRAGMENT_SHADER, fragment_shader_source);
  if (!fragment_shader.Check()) {
    throw std::runtime_error("Could not create Screen: fragment shader compilation failed");
  }

  glAttachShader(program_, vertex_shader);
  glAttachShader(program_, fragment_shader);
  glBindAttribLocation(program_, kPos2DAttribLocation, "a_pos2d");
  if (!program_.Link()) {
    throw std::runtime_error("Could not create Screen: program linkage failed");
  }
  glDetachShader(program_, vertex_shader);
  glDetachShader(program_, fragment_shader);

  InitVertices();

  GLint sampler_location = glGetUniformLocation(program_, "tex");
  glProgramUniform1i(program_, sampler_location, texture_unit);
}

Screen::~Screen() {
  glDeleteBuffers(1, &vertices_pos2d_buffer_);
  glDeleteVertexArrays(1, &vertices_);
}

void Screen::InitVertices() {
  glGenBuffers(1, &vertices_pos2d_buffer_);
  glBindBuffer(GL_ARRAY_BUFFER, vertices_pos2d_buffer_);

  GLfloat vertices_pos2d[] = {
    -1, 1,
    1, 1,
    -1, -1,
    1, -1,
  };
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices_pos2d), vertices_pos2d, GL_STATIC_DRAW);

  glGenVertexArrays(1, &vertices_);
  glBindVertexArray(vertices_);
  glVertexAttribPointer(kPos2DAttribLocation, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
  glEnableVertexAttribArray(kPos2DAttribLocation);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);
}

void Screen::Draw() {
  glUseProgram(program_);
  glBindVertexArray(vertices_);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  glBindVertexArray(0);
}
