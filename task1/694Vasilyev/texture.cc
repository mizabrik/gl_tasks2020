#include "texture.h"

#include <GL/glew.h>
#include <SOIL2.h>

#include <iostream>
Texture::Texture(GLsizei width, GLsizei height) 
  : width_(width),
    height_(height) {
  glGenTextures(1, &name_);
  glBindTexture(GL_TEXTURE_2D, name_);
  glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32F, width, height);
}

Texture::Texture(const char *filename) {
  int w, h, c;
  auto data = SOIL_load_image(filename, &w, &h, &c, SOIL_LOAD_RGBA);
  width_ = w;
  height_ = h;
  name_ = SOIL_create_OGL_texture(data, &w, &h, 4, 0, 0);
  std::cout << name_ << std::endl;
}

Texture::~Texture() {
  glDeleteTextures(1, &name_);
}

void Texture::Bind(GLuint texture_unit) {
  glActiveTexture(GL_TEXTURE0 + texture_unit);
  glBindTexture(GL_TEXTURE_2D, name_);
  std::cout << name_  << ' ' << texture_unit << std::endl;
}

void Texture::BindImage(GLuint image_unit) {
  glBindImageTexture(image_unit, name_, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F);
}

GLsizei Texture::Width() {
  return width_;
}

GLsizei Texture::Height() {
  return height_;
}
