#include "gl_helpers.h"

#include <iostream>
#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>


GLFWScope::GLFWScope() {
  ok_ = glfwInit() == GLFW_TRUE;
}

GLFWScope::~GLFWScope() {
  if (ok_) {
    glfwTerminate();
  }
}

GLFWScope::operator bool() {
  return ok_;
}

Shader::Shader(GLenum type, const char *source) {
  name_ = glCreateShader(type);
  glShaderSource(name_, 1, &source, nullptr);
  glCompileShader(name_);
}

Shader::~Shader() {
  glDeleteShader(name_);
}

bool Shader::Check() {
  GLint is_compiled = 0;
  glGetShaderiv(name_, GL_COMPILE_STATUS, &is_compiled);

  if (is_compiled == GL_FALSE) {
    GLint max_length = 0;
    glGetShaderiv(name_, GL_INFO_LOG_LENGTH, &max_length);

    std::vector<GLchar> error_log(max_length);
    glGetShaderInfoLog(name_, max_length, nullptr, error_log.data());
    std::cerr << error_log.data() << std::endl;
  }

  return is_compiled == GL_TRUE;
}

Shader::operator GLuint() {
  return name_;
}

Program::Program() {
  name_ = glCreateProgram();
}

Program::~Program() {
  glDeleteProgram(name_);
}

bool Program::Link() {
  glLinkProgram(name_);

  GLint is_linked;
  glGetProgramiv(name_, GL_LINK_STATUS, &is_linked);
  if (is_linked == GL_FALSE) {
    GLint max_length = 0;
    glGetProgramiv(name_, GL_INFO_LOG_LENGTH, &max_length);

    std::vector<GLchar> info_log(max_length);
    glGetProgramInfoLog(name_, max_length, nullptr, info_log.data());
    std::cerr << info_log.data() << '\n';
  }

  return is_linked == GL_TRUE;
}

Program::operator GLuint() {
  return name_;
}

bool ComputeShaderSupported() {
  return GLEW_VERSION_4_3 || (GLEW_ARB_compute_shader
                              && GL_ARB_shader_storage_buffer_object);
}
