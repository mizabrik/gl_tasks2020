#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <iostream>
#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/quaternion.hpp>
#include <glm/vec3.hpp>

#include "gl_helpers.h"
#include "gui.h"
#include "raytracer.h"
#include "screen.h"
#include "texture.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>
#include <glm/gtc/matrix_transform.hpp>


const int kWindowWidth = 800;
const int kWindowHeight = 600;
const char *kWindowTitle = "Trivial Ray Tracer";

const GLuint kCanvasTextureUnit = 0;
const GLuint kCanvasImageUnit = 3;
const GLuint kTextureUnit = 1;

#include "objects.inc"

void GLFWErrorCallback(int error, const char *description) {
  std::cerr << "GLFW error occured: " << description
            << " (" << error << ")" << std::endl;
}

#define DEBUG
#ifdef DEBUG
void GLAPIENTRY
MessageCallback( GLenum source,
                 GLenum type,
                 GLuint id,
                 GLenum severity,
                 GLsizei length,
                 const GLchar* message,
                 const void* userParam )
{
  fprintf( stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
           ( type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "" ),
            type, severity, message );
}
#endif

int main() {
  glfwSetErrorCallback(GLFWErrorCallback);

  GLFWScope glfw_scope;
  if (!glfw_scope) {
    std::cerr << "GLFW initialization failed.\n";
    return EXIT_FAILURE;
  }

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
#ifdef DEBUG
  glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);  
#endif
  GLFWwindow *window = glfwCreateWindow(kWindowWidth, kWindowHeight,
                                        kWindowTitle,
                                        NULL, NULL);
  if (!window) {
    std::cerr << "Window creation failed.\n";
    return EXIT_FAILURE;
  }
  glfwMakeContextCurrent(window);

  glewExperimental = GL_TRUE;
  if (GLenum err = glewInit(); err != GLEW_OK) {
    std::cerr << "GLEW initialization failed: " << glewGetErrorString(err) << '\n';
    return EXIT_FAILURE;
  }
  if (!ComputeShaderSupported()) {
    std::cerr << "Compute shaders are not available\n";
    return EXIT_FAILURE;
  }

#ifdef DEBUG
  glEnable(GL_DEBUG_OUTPUT);
  glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
  glDebugMessageCallback(MessageCallback, NULL);
  glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);
#endif

  Texture canvas(kWindowWidth, kWindowHeight);
  Texture texture("694VasilyevData1/texture.jpg");

  texture.Bind(kTextureUnit);
  canvas.Bind(kCanvasTextureUnit);
  canvas.BindImage(kCanvasImageUnit);

  int aliasing = 1;
  Camera camera{
    4.0, 3.0,
    glm::vec3(0.0f, 0.0f, 0.0f),
    glm::quat(1.0f, 0.0f, 0.0f, 0.0f)
  };

  Screen screen(kCanvasTextureUnit);
  //Screen screen(kTextureUnit);
  GUI gui(window, aliasing);
  RayTracer tracer(kTextureUnit, kCanvasImageUnit, aliasing, camera);

  tracer.SetObjects(objects);

  while (!glfwWindowShouldClose(window)) {
    gui.NewFrame();

    if (gui.UpdateCamera(camera)) {
      tracer.SetCamera(camera);
    }
    if (gui.UpdateAliasing(aliasing)) {
      tracer.SetAliasing(aliasing);
    }

    glClear(GL_COLOR_BUFFER_BIT);
    tracer.Trace(kWindowWidth, kWindowHeight);
    screen.Draw();
    gui.Render();
    glfwSwapBuffers(window);
  }
}
