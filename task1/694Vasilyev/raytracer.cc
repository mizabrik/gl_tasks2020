#include "raytracer.h"

#include <cmath>
#include <stdexcept>
#include <vector>

#include <glm/mat3x3.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "gl_helpers.h"

#include "raytracer.inc"

glm::mat4 MakeTransform(glm::vec3 position, glm::vec3 scale, glm::quat rot) {
  glm::mat4 transform = glm::mat4_cast(glm::inverse(rot));
  transform = glm::scale(transform, glm::vec3(1/scale.x, 1/scale.y, 1/scale.z));
  transform = glm::translate(transform, -position);
  return transform;
}

glm::mat4 MakeTransform(glm::vec3 position, glm::quat rot) {
  return MakeTransform(position, glm::vec3(1), rot);
}

RayTracer::RayTracer(GLuint tex_unit, GLuint canvas_image_unit, GLint aliasing, const Camera &camera) {
  Shader compute_shader(GL_COMPUTE_SHADER, kComputeShaderSource);
  if (!compute_shader.Check()) {
    throw std::runtime_error("Could not create RayTracer: compute shader compilation failed");
  }

  glAttachShader(program_, compute_shader);
  if (!program_.Link()) {
    throw std::runtime_error("Could not create RayTracer: program linkage failed");
  }
  glDetachShader(program_, compute_shader);
  
  GLint sampler_location = glGetUniformLocation(program_, "tex");
  glProgramUniform1i(program_, sampler_location, tex_unit);

  GLint canvas_location = glGetUniformLocation(program_, "canvas");
  glProgramUniform1i(program_, canvas_location, canvas_image_unit);

  aliasing_loc_ = glGetUniformLocation(program_, "aliasing");
  SetAliasing(aliasing);

  position_loc_ = glGetUniformLocation(program_, "position");
  camera_loc_ = glGetUniformLocation(program_, "camera");
  SetCamera(camera);

  glGenBuffers(1, &ssbo_);
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo_);
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

RayTracer::~RayTracer() {
  glDeleteBuffers(1, &ssbo_);
}

void RayTracer::Trace(GLuint width, GLuint height) {
    glUseProgram(program_);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, ssbo_);

    glDispatchCompute(width, height, 1);
    glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
    glUseProgram(0);
}

void RayTracer::SetObjects(const std::vector<Parabaloid> &objects) {
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo_);
  glBufferData(GL_SHADER_STORAGE_BUFFER,
               sizeof(Parabaloid) * objects.size(), objects.data(),
               GL_STATIC_DRAW);
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

void RayTracer::SetAliasing(GLint aliasing) {
  glProgramUniform1i(program_, aliasing_loc_, aliasing);
}

void RayTracer::SetCamera(const Camera &camera) {
  glProgramUniform3fv(program_, position_loc_, 1,
                      glm::value_ptr(camera.position));

  glm::mat3x3 camera_transform(1.0f);
  camera_transform *= glm::mat3_cast(camera.rotation);
  camera_transform[0] *= camera.width / 2;
  camera_transform[1] *= camera.height / 2;

  glProgramUniformMatrix3fv(program_, camera_loc_, 1, GL_FALSE,
                            glm::value_ptr(camera_transform));
}
