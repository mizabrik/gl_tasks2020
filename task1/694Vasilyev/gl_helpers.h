#ifndef GL_HELPERS_H_
#define GL_HELPERS_H_

#include <GL/glew.h>

void GLFWErrorCallback(int error, const char *description);

class GLFWScope {
 public:
  GLFWScope();
  ~GLFWScope();
  
  operator bool(); 

 private:
  bool ok_;
};

class Shader {
 public:
  Shader(GLenum type, const char *source);
  ~Shader();

  bool Check();

  operator GLuint();

 private:
  GLuint name_;
};

class Program {
 public:
  Program();
  ~Program();

  bool Link();

  operator GLuint();

 private:
  GLuint name_;
};

bool ComputeShaderSupported();

#endif // GL_HELPERS_H_
