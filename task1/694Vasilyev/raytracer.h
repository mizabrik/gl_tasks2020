#ifndef RAYTRACER_H_
#define RAYTRACER_H_

#include <vector>

#include "gl_helpers.h"

#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>

struct Parabaloid {
  glm::mat4 transform;
  float a2;
  float b2;
  glm::vec2 tex_start;
  glm::vec2 tex_size;
  float __pad[2];
};

glm::mat4 MakeTransform(
    glm::vec3 position,
    glm::vec3 scale = glm::vec3(1.0f),
    glm::quat rot = glm::quat(1.0f, 0.0f, 0.0f, 0.0f));

glm::mat4 MakeTransform(glm::vec3 position, glm::quat rot);

struct Camera {
  // "Screen" sizes.
  float width;
  float height;

  // Location of the "eye".
  glm::vec3 position;

  // Rotation of camera. By default, vector connecting "eye" with the centre
  // of the screen is (0, 0, 1).
  glm::quat rotation;
};

class RayTracer {
 public:
  RayTracer(GLuint tex_unit, GLuint canvas_image_unit, GLint aliasing, const Camera &camera);
  ~RayTracer();

  void SetObjects(const std::vector<Parabaloid> &objects);

  void SetAliasing(GLint aliasing);
  void SetCamera(const Camera &camera);

  void Trace(GLuint width, GLuint height);

 private:
  Program program_;

  GLint aliasing_loc_;
  GLint position_loc_;
  GLint camera_loc_;

  GLuint ssbo_;
};

#endif // RAYTRACER_H_
