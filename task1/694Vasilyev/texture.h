#ifndef TEXTURE_H_
#define TEXTURE_H_

#include <GL/glew.h>

class Texture {
 public:
  Texture(GLsizei width, GLsizei height);
  Texture(const char *filename);

  ~Texture();

  void Bind(GLuint texture_unit);
  void BindImage(GLuint image_unit);

  GLsizei Width();
  GLsizei Height();

 private:
  GLuint name_;
  GLsizei width_;
  GLsizei height_;
};

#endif // TEXTURE_H_
