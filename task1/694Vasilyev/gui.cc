#include "gui.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <imgui.h>

#include "imgui_impl.h"
#include "raytracer.h"

const float kMovementSpeed = 0.042;
const float kRotationSpeed = 0.01;

GUI::GUI(GLFWwindow *window, int aliasing)
  : window_(window), 
    aliasing_(aliasing) {
  ImGui_ImplGlfwGL3_Init(window_, true);  

  ImGui::GetStyle().WindowRounding = 0;
  ImGui::GetStyle().Colors[ImGuiCol_WindowBg] = ImVec4(0.42f, 0.42f, 0.42f, 0.70f);
}

GUI::~GUI() {
  ImGui_ImplGlfwGL3_Shutdown();  
}

void GUI::NewFrame() {
    glfwPollEvents();
    ImGui_ImplGlfwGL3_NewFrame();
}

bool GUI::UpdateCamera(Camera &camera) {
  const ImGuiIO &io = ImGui::GetIO();
  bool updated = false;

  if (!io.WantCaptureKeyboard) {
    glm::vec3 movement(0.0f, 0.0f, 0.0f);
    if (io.KeysDown[GLFW_KEY_W]) {
      movement.z += kMovementSpeed;
    }
    if (io.KeysDown[GLFW_KEY_A]) {
      movement.x -= kMovementSpeed;
    }
    if (io.KeysDown[GLFW_KEY_S]) {
      movement.z -= kMovementSpeed;
    }
    if (io.KeysDown[GLFW_KEY_D]) {
      movement.x += kMovementSpeed;
    }
    if (io.KeyShift) {
      movement *= 2;
    }
    if (io.KeyCtrl) {
      movement *= 0.5;
    }
    if (glm::length(movement) > 0) {
      camera.position += camera.rotation * movement;
      updated = true;
    }
  }

  if (!io.WantCaptureMouse) {
    glfwSetInputMode(window_, GLFW_CURSOR,
                     io.MouseDown[0] ? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL);

    static const glm::vec3 x_axis(0.0f, 1.0f, 0.0f);
    if (float x_delta = io.MouseDelta.x; io.MouseDown[0] && x_delta != 0) {
      camera.rotation *= glm::angleAxis(kRotationSpeed * x_delta, x_axis);
      updated = true;
    }

    static const glm::vec3 y_axis(1.0f, 0.0f, 0.0f);
    if (float y_delta = io.MouseDelta.y; io.MouseDown[0] && y_delta != 0) {
      camera.rotation *= glm::angleAxis(kRotationSpeed * y_delta, y_axis);
      updated = true;
    }
  }

  return updated;
}

bool GUI::UpdateAliasing(int &aliasing) {
  if (aliasing == aliasing_)
    return false;

  aliasing = aliasing_;
  return true;
}

void GUI::Render() {
    ImGui::SetNextWindowSize(ImVec2(800, 35), ImGuiSetCond_Once);
    ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_Once);
    ImGui::Begin("GUI", nullptr, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);

    auto framerate = ImGui::GetIO().Framerate;
    ImGui::Text("%.1f FPS (%.3f ms/frame)", framerate, 1000.0f / framerate);
    ImGui::SameLine();

    ImGui::SliderInt("anti-aliasing", &aliasing_, 1, 4);

    ImGui::End();

    ImGui::Render();
}
