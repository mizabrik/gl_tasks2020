#ifndef GUI_H_
#define GUI_H_

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "raytracer.h"

class GUI {
 public:
  GUI(GLFWwindow *window, int aliasing);
  ~GUI();

  void NewFrame();

  bool UpdateCamera(Camera &camera);
  bool UpdateAliasing(int &aliasing);

  void Render();

 private:
  GLFWwindow *window_;
  int aliasing_;
};


#endif // GUI_H_
