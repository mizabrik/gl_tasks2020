std::vector<Parabaloid> objects = {
  {
    MakeTransform(glm::vec3(-3.0f, 0.0f, 6.0f), glm::quat(0.0, glm::vec3(0, 0, 1))),
    1, 1,
    glm::vec2(2489.0/3507, 0.0),
    glm::vec2(1018.0/3507, 661.0/2479)
  },
  {
    MakeTransform(glm::vec3(0.0f, 0.0f, 6.0f), glm::quat(1.0, glm::vec3(0, 0, 1))),
    1, 1,
    glm::vec2(2489.0/3507, 0.0),
    glm::vec2(1018.0/3507, 661.0/2479)
  },
  {
    MakeTransform(glm::vec3(+3.0f, 0.0f, 6.0f), glm::quat(2.0, glm::vec3(0, 0, 1))),
    1, 1,
    glm::vec2(2489.0/3507, 0.0),
    glm::vec2(1018.0/3507, 661.0/2479)
  },
  {
    MakeTransform(glm::vec3(+0.0f, 3.0f, 6.0f), glm::vec3(5, 1, 1)),
    1, 1,
    glm::vec2(2489.0/3507, 0.0),
    glm::vec2(1018.0/3507, 661.0/2479)
  },
  {
    MakeTransform(glm::vec3(+0.0f, -3.0f, 6.0f),
                  glm::vec3(5, 1, 1), glm::quat(M_PI / 4, glm::vec3(1, 0, 0))),
    1, 1,
    glm::vec2(2458.0/3507, 691.0/2479),
    glm::vec2(1049.0/3507, 1500.0/2479)
  },
  {
    MakeTransform(glm::vec3(+0.0f, 7.0f, 6.0f)),
    1, 2,
    glm::vec2(2489.0/3507, 0.0),
    glm::vec2(1018.0/3507, 661.0/2479)
  },
};
