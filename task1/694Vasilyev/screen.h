#ifndef SCREEN_H_
#define SCREEN_H_

#include <GL/glew.h>

#include "gl_helpers.h"

class Screen {
 public:
  Screen(GLuint texture_unit);
  ~Screen();

  void Draw();

 private:
  void InitVertices();

  Program program_;

  GLuint vertices_pos2d_buffer_;
  GLuint vertices_;
};


#endif // SCREEN_H_
