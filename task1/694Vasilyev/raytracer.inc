static const char *kComputeShaderSource = R"(#version 420 core
#extension GL_ARB_compute_shader : require
#extension GL_ARB_shader_storage_buffer_object : require

layout(local_size_x = 1) in;

uniform sampler2D tex;
layout(rgba32f) uniform image2D canvas;

uniform vec3 position;
uniform mat3 camera;
uniform int aliasing = 2;

struct Parabaloid {
  mat4 transform;
  float a2;
  float b2;
  vec2 tex_start;
  vec2 tex_size;
};

layout(std430, binding = 0) buffer Objects {
  Parabaloid objects[];
};

vec4 Trace(vec3 source, vec3 direction) {
  float min_t = -1.0;
  vec4 color = vec4(0, 0, 0, 1);

  for (int i = 0; i < objects.length(); ++i) {
    // We care only about t, so we can transform our ray.
    vec3 src = (objects[i].transform * vec4(source, 1)).xyz;
    vec3 ray = mat3(objects[i].transform) * direction;

    // z = y^2/b^2 - x^2/a^2
    // z_0 + t dz = (y_0 + t dy)^2/b^2 - (x_0 + t dx)^2/a^2
    // a = dx^2/a^2 - dy^2/b^2
    // b = dz + 2x_0dx/a^2 - 2y_0dy/b^2
    // c = z_0 + x_0^2/a^2 - y_0^2/a^2
    vec2 tmp = vec2(objects[i].a2, -objects[i].b2);
    float a = dot(ray.xy, tmp * ray.xy);
    float b = ray.z + 2 * dot(ray.xy, tmp * src.xy);
    float c = src.z + dot(src.xy, tmp * src.xy);

    float d = b*b - 4*a*c;
    if (d > 0) {
      float t = 0.5 * (-b - sqrt(d)) / a;
      vec2 pt = src.xy + t * ray.xy;
      if (t > 0 && (min_t < 0 || t < min_t) &&
          clamp(pt, -1, 1) == pt) {
        min_t = t;
        color = texture(tex, objects[i].tex_start + objects[i].tex_size * 0.5 * (1 + pt));
      }
      t = 0.5 * (-b + sqrt(d)) / a;
      pt = src.xy + t * ray.xy;
      if (t > 0 && (min_t < 0 || t < min_t) &&
          clamp(pt, -1, 1) == pt) {
        min_t = t;
        color = texture(tex, objects[i].tex_start + objects[i].tex_size * 0.5 * (1 + pt));
      }
    }
  }

  return color;
}

void main() {
  ivec2 coord = ivec2(gl_GlobalInvocationID.xy);

  vec4 color = vec4(0.0, 0.0, 0.0, 0.0);

  ivec2 subsize = aliasing * ivec2(gl_NumWorkGroups.xy);
  for (int i = 0; i < aliasing; ++i) {
    for (int j = 0; j < aliasing; ++j) {
      vec3 screen_point = vec3(-1, -1, 1);
      vec2 subpixel = aliasing * gl_GlobalInvocationID.xy + vec2(i, j);
      screen_point.xy += (1 + 2 * subpixel) / subsize;
      screen_point = camera * screen_point;
      color += Trace(position, screen_point) / (aliasing*aliasing);
    }
  }

  imageStore(canvas, coord, color);
}
)";
